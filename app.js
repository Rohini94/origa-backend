const express = require('express');
var app= express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const { mongoose} = require('./db.js');

const userRoutes = require('./api/routes/user');
const orderRoutes = require('./api/routes/order');



app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/users', userRoutes);
app.use('/orders', orderRoutes);

app.use((req,res,next) =>{
    const error = new Error('Not found');
    error.status= 404;
    next(error);

})

 app.use((error,req,res,next)=>{
     res.status(error.status || 500);
     res.json({
         error: {
             message : error.message
         }

     });
 });



module.exports = app;