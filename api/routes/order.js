const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order');
const user = require('../models/user');
const User = require('../models/user');


router.get('/',(req,res,next) => {
    Order.find()
    //  .select("user subTotal date _id")
     .populate('user')
    .exec().
    then(docs => {
        const response = {
            count: docs.length,
            orders: docs.map(doc=>{
                return{
                    orderId:doc._id,
                    user:doc.user,
                    subTotal:doc.subTotal,
                    date:doc.date,
                    request:{
                        type: 'GET',
                        url: 'http://localhost:5000/orders'+ doc._id
                    }
                  }
                console.log('user');
            })
           
        };
          
        res.status(200).json(response);
        
       
    }).catch(err =>{
        console.log(err);
        res.status(500).json({error:err})

    });
});


router.get('/byUser',(req,res,next) => {
    Order.find()
    .select("user subTotal date")
   .populate(User) 
    .exec().
    then(docs => {
        const response = {
            count: docs.length,
            orders: docs.map(doc=>{
                return{
                    userId:doc.user,
                    subTotal:doc.subTotal,
                    date:doc.date,
                    request:{
                        type: 'GET',
                        url: 'http://localhost:5000/orders'+ doc._id
                    }
                  }
                
            })
        };
          
        res.status(200).json(response);
        
       
    }).catch(err =>{
        console.log(err);
        res.status(500).json({error:err})

    });
});




router.post('/',(req,res,next)=>{
    User.findById(req.body.userId)
    .then(user =>{
        if(!user){
            return res.status(404).json({
                message:"User Not Found"
            })
        }
    })
    .then(user =>{
        const order = new Order({
            _id: new mongoose.Types.ObjectId,
             user:req.body.userId,
            subTotal:req.body.subTotal,
            date:req.body.date,
    
    });
     return order
    .save()
})
    .then(result =>{
        console.log(result);
        res.status(201).json({
            message:'order saved Sucessfully',
            CreatedOrder:{
                _id:result._id,
            user:result.user,
            subTotal:result.subTotal,
            date:result.date
            }
        });
    })
 

    .catch(err =>{
        res.status(500).json({
            message:"User Not Found",
            error:err
        })
    });
});


// router.get('/:orderId',(req,res,next) =>{
//     const id = req.params.orderId;
//     Order.findById(id)
//     .select("user subTotal date")
//     .exec().then(doc =>{
//         console.log(doc);
//         if(doc){
//         res.status(200).json({

//             order:{doc,
//                 noOfOrders,
//             request:{
//                 type: 'GET',
//                 url: 'http://localhost:5000/order'+ doc._id
//             }}
//         });
//         }
//         else{
//             res.status(404).json({message:'record not found'});
//         }
//     }).catch(err =>{
//         console.log(err);
//         res.status(500).json({error:err})

//     });
// })

// router.get('  ',(req,res,next) =>{
//     const id = req.params.userId;
//     Order.findById(id)
//     .select("user subTotal date")
//     .exec().then(doc =>{
//         console.log(doc);
//         if(doc){
//         res.status(200).json({
        
//             order:{doc,
//             request:{
//                 type: 'GET',
//                 url: 'http://localhost:5000/order'+ doc._id
//             }}
//         });
//         }
//         else{
//             res.status(404).json({message:'record not found'});
//         }
//     }).catch(err =>{
//         console.log(err);
//         res.status(500).json({error:err})

//     });
// })

router.patch('/:id',(req,res,next)=>{
    const id = req.params.orderId;
    const Opschang = {};
    for(const ops of req.body){
        Opschang[ops.propName] = ops.value;
    }
   Order.update( {_id:id} , { $set :Opschang })
//    {userId:req.body.userId, name:req.body.name}
   .exec()
   .then(result=>{
       res.status(200).json({result,
    message:"record updated sucessfully"});

   }).catch(err=>{
       res.status(500).json({error:err});
   });
});

router.delete('/:id',(req,res,next)=>{
    const id = req.params.id;
    Order.remove({_id:id}).exec().then(result=>{
        res.status(200).json(result)
    }).catch(err =>{
        res.status(500).json({error:err})
    });
});



//new code start 



// Handle incoming GET requests to /orders
router.get("/count", (req, res, next) => {
    Order.find()
      .select(" userId subTotal date")
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          orders: docs.map(doc => {
            return {
              userId: doc.user,
              subTotal:doc.subTotal,
              date:doc.date,
              _id: doc._id,
              request: {
                type: "GET",
                url: "http://localhost:3000/users/" + doc._id
              }
            };
          })
        };
        //   if (docs.length >= 0) {
        res.status(200).json(response);
        //   } else {
        //       res.status(404).json({
        //           message: 'No entries found'
        //       });
        //   }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });
  
  
  router.post("/user", (req, res, next) => {
    User.findById(req.body.UserId)
      .then(user => {
        if (!user) {
          return res.status(404).json({
            message: "user not found"
          });
        }
        const order = new Order({
          _id: mongoose.Types.ObjectId(),
          userId: req.body.userId,
          subTotal: req.body.subTotal,
          date:req.body.date
        });
        return order.save();
      })
      .then(result => {
        console.log(result);
        res.status(201).json({
          message: "Order stored",
          createdOrder: {
            _id: result._id,
            userId: result.userId,
            subTotal: result.subTotal,
            date:result.date
          },
          request: {
            type: "GET",
            url: "http://localhost:3000/orders/" + result._id
          }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });
  
  router.get("/:orderId", (req, res, next) => {
    Order.findById(req.params.orderId)
      .exec()
      .then(order => {
        if (!order) {
          return res.status(404).json({
            message: "Order not found"
          });
        }
        res.status(200).json({
          order: order,
          request: {
            type: "GET",
            url: "http://localhost:3000/orders"
          }
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });
  
  router.delete("/:orderId", (req, res, next) => {
    Order.remove({ _id: req.params.orderId })
      .exec()
      .then(result => {
        res.status(200).json({
          message: "Order deleted",
          request: {
            type: "POST",
            url: "http://localhost:3000/orders",
            body: { productId: "ID", quantity: "Number" }
          }
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });
  
  module.exports = router;
//new code end
module.exports = router;