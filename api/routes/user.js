const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = require('../models/user');
 const Order = require('../models/order');
// const user = require('../models/user');



router.get("/byName", (req, res, next) => {
  Order.find()
    // .select("user subTotal date _id")
    .exec()
    .then(docs => {
      res.status(200).json({
        count: docs.length,
        orders: docs.map(doc => {
          return {
            _id: doc._id,
            userId: doc.user,
            subTotal: doc.subTotal,
            date:doc.date,
            request: {
              type: "GET",
              url: "http://localhost:5000/orders/" + doc._id
            }
          };
        })
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
});








// run method count by id  start

// router.get("/:id", (req, res, next) => {
//   const id = req.params.id;
//   User.findById(id)
//     .select('name _id')
//     .exec()
//     .then(doc => {
//       console.log("From database", doc);
//       if (doc) {
//         res.status(200).json({
//             product: doc,
//             request: {
//                 type: 'GET',
//                 url: 'http://localhost:5000/users'
//             }
//         });
//       } else {
//         res
//           .status(404)
//           .json({ message: "No valid entry found for provided ID" });
//       }
//     })
//     .catch(err => {
//       console.log(err);
//       res.status(500).json({ error: err });
//     });
// });


router.get('/',(req,res,next) => {
    
    User.find()
    // .select(" _id name")
    .exec().
    then(docs => {
        const response = {
            users: docs.map(doc=>{
                return{
                    _id:doc._id,
                    name:doc.name,
                    request:{
                        type: 'GET',
                        url: 'http://localhost:5000/users'
                        + doc._id
                    }
                  }
                
            })
        };
          
        res.status(200).json(docs);
        
       
    }).catch(err =>{
        console.log(err);
        res.status(500).json({error:err})

    });
});

router.get('/byOrder',(req,res,next) => {
    
    User.find()
    // .select(" _id name")
    .populate('user')
    .exec().
    then(docs => {
        const response = {
            users: docs.map(doc=>{
                return{
                    _id:doc._id,
                    name:doc.name,
                    // order:doc.id,

                    request:{
                        type: 'GET',
                        url: 'http://localhost:5000/users'+ doc._id
                    }
                  }
                
            })
        };
          
        res.status(200).json(docs);
        
       
    }).catch(err =>{
        console.log(err);
        res.status(500).json({error:err})

    });
});

//runable start

router.post('/',(req,res,next)=>{
 
    const user = new User({
        _id: new mongoose.Types.ObjectId,
       
        name:req.body.name

});
user.save().then(result =>{
    console.log(result);
    res.status(201).json({
        message:"user created sucessfully",
        createdUser:result

    });
})
.catch(err => {
    console.log(err);

})
    res.status(201).json({
        message:'user was created',
        user:user

    });
});



    router.patch("/:UserId", (req, res, next) => {
      const id = req.params.productId;
      const updateOps = {};
      for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
      }
      Product.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
          res.status(200).json({
              message: 'Product updated',
              request: {
                  type: 'GET',
                  url: 'http://localhost:3000/products/' + id
              }
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
    });

router.put('/:id',(req,res,next)=>{
    const id = req.params.id;
    
   User.update( {_id:id} , { $set :{orderId:req.body.userId, name:req.body.name} })
//    {userId:req.body.userId, name:req.body.name}
   .exec()
   .then(result=>{
       res.status(200).json(result);

   }).catch(err=>{
       res.status(500).json({error:err});
   });
});

router.delete('/:id',(req,res,next)=>{
    const id = req.params.id;
    User.remove({_id:id}).exec().then(result=>{
        res.status(200).json(result)
    }).catch(err =>{
        res.status(500).json({error:err})
    });
});






module.exports = router;