const mongoose = require('mongoose');

var userSchema = mongoose.Schema({
     _id:mongoose.Schema.Types.ObjectId,
    
    //  order:{type:mongoose.Schema.Types.ObjectId ,ref:'order',required:true},
//    userId:{
//     type:Number,
//     required:true
//  },
   name: {
    type:String,
    required:true
 },
 no_Of_Orders:{
     type:Number,
     default:0
 }
});


module.exports = mongoose.model('user',userSchema);

