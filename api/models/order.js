const mongoose = require('mongoose');
const User = require('../models/user');


var orderSchema = mongoose.Schema({
    
   _id:mongoose.Schema.Types.ObjectId,
   user : {type:mongoose.Schema.Types.ObjectId , ref: 'user' ,required:true},

   subTotal:{
    type:Number,
    required:true
 },
   date:{
    type:Date,
    required:true
 }
});


orderSchema.post('save',function(doc)
{
   console.log('doc.user',doc.user);
   User.update({'_id':doc.user},{$inc:{'no_Of_Orders':+1}})
   .exec(function(err,updaterow){
      console.log('updaterow',updaterow);

      if(err){
         console.log(err)

         // res.status(404).json(err);

      }
      else{
         // res.status(200).json(updaterow);

      }
      
   })

})

orderSchema.post('save',function(doc)
{
   console.log('doc.avgBillValue',doc.avgBillValue);
   User.update({ avgBillValue: { $avg: { $multiply: [ "$subTotal", "$no_Of_Orders" ] } }})
   .exec(function(err,updaterow){
      console.log('updaterow',updaterow);

      if(err){
         console.log(err)

         // res.status(404).json(err);

      }
      else{
         // res.status(200).json(updaterow);

      }
      
   })

})

// orderSchema.update('')


module.exports = mongoose.model('order',orderSchema);
